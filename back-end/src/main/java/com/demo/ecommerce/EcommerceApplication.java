package com.demo.ecommerce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.*;

@SpringBootApplication
@RestController
public class EcommerceApplication {
    public static void main(String[] args) {
        SpringApplication.run(MgnDemoApplication.class, args);
    }

    @GetMapping("/hello")
    public String hello(){
        return "Spring is here!";
    }

    @GetMapping("/activities")
    public List<String> getActivities(){
        List<String> activities = new ArrayList<String>();
        activities.add("act1");
        activities.add("act2");
        activities.add("act3");
        return  activities;
    }
}
